import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async validateUser(token, profile): Promise<any> {
    let user = await this.usersService.findOne(profile.id);

    if (!user) {
      user = await this.usersService.create(token, profile);
    }

    return user;
  }
}
