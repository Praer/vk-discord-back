import { Strategy } from 'passport-vkontakte';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, Module } from '@nestjs/common';
import { AuthService } from './auth.service';

import { ConfigModule } from '@nestjs/config';

@Injectable()
@Module({
  imports: [ConfigModule.forRoot()],
})
export class VkontakteStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      clientID: process.env.VKONTAKTE_APP_ID,
      clientSecret: process.env.VKONTAKTE_APP_SECRET,
      callbackURL: process.env.VKONTAKTE_APP_CALLBACK,
      scope: ['status', 'offline'],
      apiVersion: '5.107',
    });
  }
  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
  ): Promise<any> {
    const user = await this.authService.validateUser(accessToken, {
      id: profile.id,
      last_name: profile.name.familyName,
      first_name: profile.name.givenName,
      url: profile.profileUrl,
    });

    return user;
  }
}
