import { Controller, Req, Res, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { VkAuthGuard } from './vk.guard';

@Controller('auth')
export class AuthController {
  @UseGuards(AuthGuard('vkontakte'))
  @Get('vkontakte')
  login() {}

  @UseGuards(VkAuthGuard)
  @Get('vkontakte/callback')
  async vkCallback(@Res() res, @Req() req) {
    const user = req.user;
    res.redirect('/user?id=' + user.id);
  }
}
